#ifndef ALIENFXCOLORS_H_
#define ALIENFXCOLORS_H_

#include "ui_AlienFXColors.h"
#include <QFile>

namespace alienfxcolors
{

class AlienFXColors: public QMainWindow
{
Q_OBJECT
public:
	AlienFXColors();
	virtual ~AlienFXColors();

public slots:
	void on_left_clicked();
	void on_center_clicked();
	void on_right_clicked();
	void on_all_clicked();
	void on_effects_currentIndexChanged(int);
	void on_red_valueChanged(int);
	void on_green_valueChanged(int);
	void on_blue_valueChanged(int);
	void effect();

private:
	QFile files[3];

	enum class Zones
	{
		LEFT = 1, CENTER = 0, RIGHT = 2
	};

	enum class Effects
	{
		NONE = 0, TRANSITION = 1, RANDOM = 2
	};

	Ui::AlienFXColorsMainWindow ui;
	Effects selectedEffect;

	QColor chooseColor();
	void changeColor(Zones which);
	void setAlienFXColor(Zones which, const QColor& color);

};

} /* namespace alienfxcolors */

#endif /* ALIENFXCOLORS_H_ */
