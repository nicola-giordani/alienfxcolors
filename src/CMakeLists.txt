# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5Core)
find_package(Qt5Widgets)

QT5_WRAP_UI(UIS_HDRS AlienFXColors.ui)

add_executable(AlienFXColors AlienFXColors.cpp ${UIS_HDRS})

set_property(TARGET AlienFXColors PROPERTY CXX_STANDARD 11)
set_property(TARGET AlienFXColors PROPERTY CXX_STANDARD_REQUIRED ON)

target_link_libraries(AlienFXColors Qt5::Core Qt5::Widgets)
