#include "AlienFXColors.h"
#include <QApplication>
#include <QColorDialog>
#include <QTimer>
#include <QDebug>

namespace alienfxcolors
{

AlienFXColors::AlienFXColors()
{
	ui.setupUi(this);
	QTimer* timer = new QTimer();
	timer->setInterval(ui.interval->value());
	connect(timer, SIGNAL(timeout()), this, SLOT(effect()));
	timer->start();
	for (int i = 0; i < 3; i++)
	{
		files[i].setFileName(
				QString("/sys/devices/platform/alienware-wmi/rgb_zones/zone%1").arg(
						i, 2, 10, QChar('0')));
		files[i].open(QFile::WriteOnly);
	}
	connect(ui.interval, SIGNAL(valueChanged(int)), timer, SLOT(start(int)));
}

AlienFXColors::~AlienFXColors()
{
	for (int i = 0; i < 3; i++)
		files[i].close();
}

void AlienFXColors::setAlienFXColor(Zones which, const QColor& color)
{
	files[(int) which].write(color.name().remove(0, 1).toStdString().c_str());
	files[(int) which].flush();
}

QColor AlienFXColors::chooseColor()
{
	QColorDialog colorDialog;
	QColor color = colorDialog.getColor(color, this, "Select a color");
	return QColor::fromRgb(color.red() / 16, color.green() / 16,
			color.blue() / 16);
}

void AlienFXColors::changeColor(Zones which)
{
	setAlienFXColor(which, chooseColor());
}

void AlienFXColors::on_left_clicked()
{
	changeColor(Zones::LEFT);
}

void AlienFXColors::on_center_clicked()
{
	changeColor(Zones::CENTER);
}

void AlienFXColors::on_right_clicked()
{
	changeColor(Zones::RIGHT);
}

void AlienFXColors::on_all_clicked()
{
	QColor color = chooseColor();
	setAlienFXColor(Zones::LEFT, color);
	setAlienFXColor(Zones::CENTER, color);
	setAlienFXColor(Zones::RIGHT, color);
}

void AlienFXColors::on_effects_currentIndexChanged(int v)
{
	selectedEffect = (Effects) v;
}

void AlienFXColors::on_red_valueChanged(int v)
{
	QColor color = QColor::fromRgb(ui.red->value(), ui.green->value(),
			ui.blue->value());
	setAlienFXColor(Zones::LEFT, color);
	setAlienFXColor(Zones::CENTER, color);
	setAlienFXColor(Zones::RIGHT, color);
}

void AlienFXColors::on_green_valueChanged(int v)
{
	QColor color = QColor::fromRgb(ui.red->value(), ui.green->value(),
			ui.blue->value());
	setAlienFXColor(Zones::LEFT, color);
	setAlienFXColor(Zones::CENTER, color);
	setAlienFXColor(Zones::RIGHT, color);
}

void AlienFXColors::on_blue_valueChanged(int v)
{
	QColor color = QColor::fromRgb(ui.red->value(), ui.green->value(),
			ui.blue->value());
	setAlienFXColor(Zones::LEFT, color);
	setAlienFXColor(Zones::CENTER, color);
	setAlienFXColor(Zones::RIGHT, color);
}

void AlienFXColors::effect()
{
	QColor color;
	static int hue = 0;
	switch (selectedEffect)
	{
	case Effects::NONE:
		return;
	case Effects::TRANSITION:
		hue = (hue + 1) % 360;
		color = QColor::fromHsv(hue, 255, 255);
		color = QColor::fromRgb(color.red() / 16, color.green() / 16,
				color.blue() / 16);
		break;
	case Effects::RANDOM:
		color = QColor::fromRgb(rand() % 16, rand() % 16, rand() % 16);
		break;
	}
	setAlienFXColor(Zones::LEFT, color);
	setAlienFXColor(Zones::CENTER, color);
	setAlienFXColor(Zones::RIGHT, color);
}

} /* namespace alienfxcolors */

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	alienfxcolors::AlienFXColors gui;
	gui.show();
	return app.exec();
}
